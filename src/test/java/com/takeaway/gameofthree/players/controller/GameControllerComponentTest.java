package com.takeaway.gameofthree.players.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.takeaway.gameofthree.players.common.dto.NumbersModel;
import com.takeaway.gameofthree.players.common.error.ApiError;
import com.takeaway.gameofthree.players.common.error.code.ApiErrorCode;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.ClassRule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/29/2020 by OLE
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = {"PlayerA_Topic"})
class GameControllerComponentTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private static final String BASE_URI = "/game/start";

    @Autowired
    private ObjectMapper objectMapper;

    @ClassRule
    static WireMockRule numbersService = new WireMockRule(2323);

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @BeforeAll
    static void start() {
        numbersService.start();
    }

    @AfterAll
    static void afterEach() {
        numbersService.stop();
    }

    @Test
    void whenStartGameSuccessfully() throws Exception {
        NumbersModel numbersModel = NumbersModel.builder()
                .number(15).build();
        numbersService.stubFor(
                WireMock.get(WireMock.urlPathMatching("/numbers/init"))
                        .willReturn(aResponse()
                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                .withBody(objectMapper.writeValueAsString(numbersModel))
                                .withStatus(200))
        );


        Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("player1", "false", embeddedKafkaBroker);
        consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        ConsumerFactory cf = new DefaultKafkaConsumerFactory<String, Integer>(consumerProps, new StringDeserializer(), new IntegerDeserializer());
        Consumer<String, Integer> consumerServiceTest = cf.createConsumer();

        embeddedKafkaBroker.consumeFromAnEmbeddedTopic(consumerServiceTest, "PlayerA_Topic");


        ResponseEntity response = restTemplate.postForEntity(BASE_URI, null, ResponseEntity.class);

        ConsumerRecord<String, Integer> consumerRecordOfExampleDTO = KafkaTestUtils.getSingleRecord(consumerServiceTest, "PlayerA_Topic");
        Integer valueReceived = consumerRecordOfExampleDTO.value();
        assertThat(valueReceived).isEqualTo(15);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void whenStartGameAndNumbersServiceReturnError_500_ThenFailAndReturnStatus_500() throws Exception {
        NumbersModel numbersModel = NumbersModel.builder()
                .number(15).build();
        numbersService.stubFor(
                WireMock.get(WireMock.urlPathMatching("/numbers/init"))
                        .willReturn(aResponse()
                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                .withBody(objectMapper.writeValueAsString(numbersModel))
                                .withStatus(500))
        );

        ResponseEntity<ApiError> response = restTemplate.postForEntity(BASE_URI, null, ApiError.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getBody().getCode()).isEqualTo(ApiErrorCode.SERVICE_UNAVAILABLE.getCode());
        assertThat(response.getBody().getMessage()).isEqualTo(ApiErrorCode.SERVICE_UNAVAILABLE.name());
    }

    @Test
    void whenStartGameWithInvalidModeThenFailed_INVALID_MODE_WithStatus_400(){
        ResponseEntity<ApiError> response = restTemplate.postForEntity(BASE_URI+"?mode=kkk", null, ApiError.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getCode()).isEqualTo(ApiErrorCode.INVALID_MODE.getCode());
        assertThat(response.getBody().getMessage()).isEqualTo(ApiErrorCode.INVALID_MODE.name());
    }
}
