package com.takeaway.gameofthree.players.controller;

import com.takeaway.gameofthree.players.common.error.code.ApiErrorCode;
import com.takeaway.gameofthree.players.component.game.GameService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/29/2020 by OLE
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = GameController.class)
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameService gameService;

    private static final String BASE_URL = "/game/start";

    @Test
    void whenStartNewGameWithValidData() throws Exception {
        willDoNothing().given(gameService).startNewGame("AUTO", null);
        mockMvc
                .perform(MockMvcRequestBuilders.post(BASE_URL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void whenStartNewGameWithInvalidRequestParamTypeThenFailWithStatus_400() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post(BASE_URL+"?number=test"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code", is(ApiErrorCode.INVALID_QUERY_PARAM_VALUE.getCode())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", is(ApiErrorCode.INVALID_QUERY_PARAM_VALUE.name())));
    }

}
