package com.takeaway.gameofthree.players.component.game;

import com.takeaway.gameofthree.players.common.clients.NumbersClient;
import com.takeaway.gameofthree.players.common.dto.NumbersModel;
import com.takeaway.gameofthree.players.common.enums.ModesEnum;
import com.takeaway.gameofthree.players.common.error.exception.InvalidModeException;
import com.takeaway.gameofthree.players.common.error.exception.InvalidNumberRangeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.BDDMockito.given;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@ExtendWith(MockitoExtension.class)
class GameServiceTest {

    @InjectMocks
    private GameServiceImpl gameServiceImpl;

    @Mock
    private NumbersClient numbersClient;

    @Test
    void whenGetStartNumberWithMode_AUTO_ThenGetARandomNumberFromNumbersService() {
        ModesEnum mode = ModesEnum.AUTO;
        Integer number = null;
        NumbersModel numbersModel = NumbersModel.builder()
                .number(150).build();
        given(numbersClient.getStartNumber()).willReturn(numbersModel);
        int result = gameServiceImpl.getStartNumber(mode, number);
        assertThat(result).isEqualTo(numbersModel.getNumber());
    }

    @Test
    void whenGetStartNumberWithMode_MANUAL_WithNullNumberThenThrow_InvalidNumberRangeException() {
        ModesEnum mode = ModesEnum.MANUAL;
        Integer number = null;
        assertThrows(InvalidNumberRangeException.class, () -> gameServiceImpl.getStartNumber(mode, number));
    }

    @Test
    void whenGetStartNumberWithMode_MANUAL_WithNumberLessThanMinRangeThenThrow_InvalidNumberRangeException() {
        ModesEnum mode = ModesEnum.MANUAL;
        Integer number = 1;
        gameServiceImpl.setMin(2);
        assertThrows(InvalidNumberRangeException.class, () -> gameServiceImpl.getStartNumber(mode, number));
    }

    @Test
    void whenGetStartNumberWithMode_MANUAL_WithNumberMoreThanMaxRangeThenThrow_InvalidNumberRangeException() {
        ModesEnum mode = ModesEnum.MANUAL;
        Integer number = 11;
        gameServiceImpl.setMin(2);
        gameServiceImpl.setMax(10);
        assertThrows(InvalidNumberRangeException.class, () -> gameServiceImpl.getStartNumber(mode, number));
    }

    @Test
    void whenStartNewGameWithInValidModeThenThrow_InvalidModeException() {
        String mode = "mode";
        assertThrows(InvalidModeException.class, () -> gameServiceImpl.startNewGame(mode, null));
    }


}
