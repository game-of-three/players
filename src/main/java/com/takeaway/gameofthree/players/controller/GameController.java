package com.takeaway.gameofthree.players.controller;

import com.takeaway.gameofthree.players.component.game.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@RestController
@RequestMapping("/game")
public class GameController implements GameApi {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/start")
    public void startNewGame(@RequestParam(required = false, defaultValue = "${game.mode.default}") String mode,
                             @RequestParam(required = false) Integer number) {
        gameService.startNewGame(mode, number);
    }
}
