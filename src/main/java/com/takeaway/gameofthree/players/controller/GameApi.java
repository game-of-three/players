package com.takeaway.gameofthree.players.controller;

import com.takeaway.gameofthree.players.common.enums.ModesEnum;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.net.HttpURLConnection;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public interface GameApi {

    @ApiOperation(value = "Start a game ")
    @ApiResponses({@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "The game initiated successfully"),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Some parameters are missing or invalid")})
    void startNewGame(
            @ApiParam(value = "The mode of the game ", allowableValues = ModesEnum.valuesList) String mode,
            @ApiParam(value = "The initial number which will be required in case the game mode is MANUAL") Integer number);
}
