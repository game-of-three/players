package com.takeaway.gameofthree.players.component.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@Service
public class PlayerProducerServiceImpl implements PlayerProducerService {

    @Value("${player.producer.topic}")
    private String TOPIC;

    private final KafkaTemplate<String, Integer> kafkaTemplate;

    @Autowired
    public PlayerProducerServiceImpl(KafkaTemplate<String, Integer> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void passToTheOtherPlayer(int startNumber) {
        kafkaTemplate.send(TOPIC, startNumber);
    }
}
