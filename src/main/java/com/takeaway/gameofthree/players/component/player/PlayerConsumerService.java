package com.takeaway.gameofthree.players.component.player;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public interface PlayerConsumerService {

    void play(Integer number);
}
