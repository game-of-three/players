package com.takeaway.gameofthree.players.component.game;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public interface GameService {
    void startNewGame(String mode, Integer number);
}
