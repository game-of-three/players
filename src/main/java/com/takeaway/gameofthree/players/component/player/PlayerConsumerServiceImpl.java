package com.takeaway.gameofthree.players.component.player;

import com.takeaway.gameofthree.players.common.dto.NumbersModel;
import com.takeaway.gameofthree.players.common.clients.NumbersClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@Service
@Slf4j
public class PlayerConsumerServiceImpl implements PlayerConsumerService {

    private NumbersClient numbersClient;

    private PlayerProducerService playerProducerService;

    @Autowired
    public PlayerConsumerServiceImpl(NumbersClient numbersClient, PlayerProducerService playerProducerService) {
        this.numbersClient = numbersClient;
        this.playerProducerService = playerProducerService;
    }

    @Override
    @KafkaListener(topics = "${player.consumer.topic}")
    public void play(Integer currentNumber) {
        NumbersModel nextNumber = numbersClient.getNextNumber(currentNumber);

        log.info("The added value is -> {} and the result of division is -> {} for the number -> {}", nextNumber.getAddedValue(), nextNumber.getNumber(), currentNumber);

        if (nextNumber.getNumber() == 1) {
            log.info("Congratulation you are the winner !!! ");
        } else {
            playerProducerService.passToTheOtherPlayer(nextNumber.getNumber());
        }

    }

}
