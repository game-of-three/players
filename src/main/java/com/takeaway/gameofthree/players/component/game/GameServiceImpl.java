package com.takeaway.gameofthree.players.component.game;

import com.takeaway.gameofthree.players.common.clients.NumbersClient;
import com.takeaway.gameofthree.players.common.enums.ModesEnum;
import com.takeaway.gameofthree.players.common.error.exception.InvalidModeException;
import com.takeaway.gameofthree.players.common.error.exception.InvalidNumberRangeException;
import com.takeaway.gameofthree.players.component.player.PlayerProducerService;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@Service
@Slf4j
public class GameServiceImpl implements GameService {

    @Value("${numbers.range.min}")
    @Setter(AccessLevel.PACKAGE)
    private Integer min;
    @Value("${numbers.range.max}")
    @Setter(AccessLevel.PACKAGE)
    private Integer max;

    private NumbersClient numbersClient;

    private PlayerProducerService playerProducerService;

    @Autowired
    public GameServiceImpl(NumbersClient numbersClient, PlayerProducerService playerProducerService) {
        this.numbersClient = numbersClient;
        this.playerProducerService = playerProducerService;
    }

    @Override
    public void startNewGame(String mode, Integer number) {
        try {
            ModesEnum modesEnum = ModesEnum.valueOf(mode);
            int startNumber = getStartNumber(modesEnum, number);
            playerProducerService.passToTheOtherPlayer(startNumber);

        } catch (IllegalArgumentException ex) {
            throw  new InvalidModeException(mode);
        }
    }

    int getStartNumber(ModesEnum mode, Integer number) {
        int startNumber = 0;
        if (mode == ModesEnum.AUTO) {
            startNumber = numbersClient.getStartNumber().getNumber();
        } else if (mode == ModesEnum.MANUAL && (number == null || number < min || number > max)) {
            throw new InvalidNumberRangeException(""+number);
        }
        return startNumber;
    }
}
