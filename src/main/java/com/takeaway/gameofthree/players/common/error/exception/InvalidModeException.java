package com.takeaway.gameofthree.players.common.error.exception;

import com.takeaway.gameofthree.players.common.error.code.ApiErrorCode;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public class InvalidModeException extends ServiceException {
    public InvalidModeException(String message) {
        super(message);
    }

    @Override
    public ApiErrorCode getApiErrorCode() {
        return ApiErrorCode.INVALID_MODE;
    }
}
