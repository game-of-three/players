package com.takeaway.gameofthree.players.common.enums;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public enum ModesEnum {

    AUTO,

    MANUAL;

    public static final String valuesList = "AUTO, MANUAL";

}
