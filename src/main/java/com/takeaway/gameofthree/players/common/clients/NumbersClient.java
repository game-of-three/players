package com.takeaway.gameofthree.players.common.clients;

import com.takeaway.gameofthree.players.common.dto.NumbersModel;
import com.takeaway.gameofthree.players.common.config.FeignClientErrorDecoder;
import com.takeaway.gameofthree.players.common.config.FeignClientRetryerConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@FeignClient(name = "NumbersClientApiIntegration", url = "${external-services.numbers.uri}", configuration = {FeignClientErrorDecoder.class, FeignClientRetryerConfig.class})
public interface NumbersClient {

    @GetMapping("/numbers/init")
    NumbersModel getStartNumber();

    @GetMapping("/numbers/next")
    NumbersModel getNextNumber(@RequestParam int number);
}
