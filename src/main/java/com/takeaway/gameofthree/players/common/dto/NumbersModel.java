package com.takeaway.gameofthree.players.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NumbersModel {
    private int number;
    private int addedValue;
}
