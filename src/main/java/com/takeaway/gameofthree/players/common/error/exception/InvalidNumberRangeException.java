package com.takeaway.gameofthree.players.common.error.exception;

import com.takeaway.gameofthree.players.common.error.code.ApiErrorCode;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public class InvalidNumberRangeException extends ServiceException {

    public InvalidNumberRangeException(String message) {
        super(message);
    }

    @Override
    public ApiErrorCode getApiErrorCode() {
        return ApiErrorCode.INVALID_NUMBER_RANGE;
    }
}
