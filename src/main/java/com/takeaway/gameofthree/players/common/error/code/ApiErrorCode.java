package com.takeaway.gameofthree.players.common.error.code;

import com.takeaway.gameofthree.players.common.error.ApiError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
public enum ApiErrorCode {

    INVALID_QUERY_PARAM_VALUE(10, HttpStatus.BAD_REQUEST, "Invalid query-string parameter value"),

    INVALID_MODE(12, HttpStatus.BAD_REQUEST, "Invalid mode value "),

    SERVICE_VALIDATION_ERROR(29, HttpStatus.BAD_REQUEST,"Invalid Data"),

    SERVICE_UNAVAILABLE(5, HttpStatus.INTERNAL_SERVER_ERROR,"The service is temporarily unavailable"),

    INVALID_NUMBER_RANGE(13, HttpStatus.BAD_REQUEST, "Number out of the defined range ");

    @Getter
    private final int code;

    @Getter
    private final HttpStatus httpStatus;

    @Getter
    private final String message;

    ApiErrorCode(int code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.message = message;
    }


    public ApiError toResponseEntity(String details) {
        return new ApiError(code, name(), message + ": " + details);
    }

    public ApiError toResponseEntity() {
        return new ApiError(code, name(), message);
    }
}
