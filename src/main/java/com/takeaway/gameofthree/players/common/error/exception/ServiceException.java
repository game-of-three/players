package com.takeaway.gameofthree.players.common.error.exception;

import com.takeaway.gameofthree.players.common.error.code.ApiErrorCode;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/28/2020 by OLE
 */
public abstract class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public abstract ApiErrorCode getApiErrorCode();
}
