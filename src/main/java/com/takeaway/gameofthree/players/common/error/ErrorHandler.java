package com.takeaway.gameofthree.players.common.error;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.takeaway.gameofthree.players.common.error.code.ApiErrorCode;
import com.takeaway.gameofthree.players.common.error.exception.ServiceException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;


/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {


    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiErrorCode errorCode = ApiErrorCode.INVALID_QUERY_PARAM_VALUE;
        String detailedMessage = ex.getMessage();
        return new ResponseEntity<>(errorCode.toResponseEntity(detailedMessage), errorCode.getHttpStatus());
    }

    @ExceptionHandler(ServiceException.class)
    public final ResponseEntity<ApiError> handleServiceException(ServiceException exception) {
        ApiErrorCode errorCode = exception.getApiErrorCode();
        String detailedMessage = exception.getMessage();
        return new ResponseEntity<>(errorCode.toResponseEntity(detailedMessage), errorCode.getHttpStatus());
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public final ResponseEntity<ApiError> handleHttpClientErrorException(HttpClientErrorException exception){
        try {
            ApiError errorResponse = new ObjectMapper().readValue(exception.getResponseBodyAsByteArray(), ApiError.class);
            return ResponseEntity.status(exception.getStatusCode())
                    .body(errorResponse);
        }catch (Exception ex){
            ApiErrorCode errorCode = ApiErrorCode.SERVICE_VALIDATION_ERROR;
            String detailedMessage = exception.getMessage();
            return new ResponseEntity<>(errorCode.toResponseEntity(detailedMessage), errorCode.getHttpStatus());
        }
    }

    @ExceptionHandler(HttpServerErrorException.class)
    public final ResponseEntity<ApiError> handleHttpServerErrorException(HttpServerErrorException exception) throws IOException {
        try {
            ApiError errorResponse = new ObjectMapper().readValue(exception.getResponseBodyAsByteArray(), ApiError.class);
            return ResponseEntity.status(exception.getStatusCode())
                    .body(errorResponse);
        }catch (Exception ex){
            ApiErrorCode errorCode = ApiErrorCode.SERVICE_UNAVAILABLE;
            String detailedMessage = exception.getMessage();
            return new ResponseEntity<>(errorCode.toResponseEntity(detailedMessage), errorCode.getHttpStatus());
        }
    }

}
