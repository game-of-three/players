package com.takeaway.gameofthree.players.common.config;

import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClientRetryerConfig {
    @Bean
    public Retryer retryer() {
        return new Retryer.Default(100,1000,5);
    }

}