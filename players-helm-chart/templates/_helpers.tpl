{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "playerA.name" -}}
{{- default .Chart.Name .Values.playerAfullnameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "playerB.name" -}}
{{- default .Chart.Name .Values.playerAfullnameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "playerA.fullname" -}}
{{- if .Values.playerAfullnameOverride }}
{{- .Values.playerAfullnameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}


{{- define "playerB.fullname" -}}
{{- if .Values.playerBfullnameOverride }}
{{- .Values.playerBfullnameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "playerA.chart" -}}
{{- printf "%s-%s" .Values.playerAfullnameOverride .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "playerB.chart" -}}
{{- printf "%s-%s" .Values.playerBfullnameOverride .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "playerA.labels" -}}
helm.sh/chart: {{ include "playerA.chart" . }}
{{ include "playerA.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "playerB.labels" -}}
helm.sh/chart: {{ include "playerB.chart" . }}
{{ include "playerB.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "playerA.selectorLabels" -}}
app.kubernetes.io/name: {{ include "playerA.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app: {{ include "playerA.fullname" .}}
{{- end }}

{{- define "playerB.selectorLabels" -}}
app.kubernetes.io/name: {{ include "playerB.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app: {{ include "playerB.fullname" .}}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "playerA.serviceAccountName" -}}
{{- if .Values.PlayerA.serviceAccount.create }}
{{- default (include "playerA.fullname" .) .Values.PlayerA.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.PlayerA.serviceAccount.name }}
{{- end }}
{{- end }}


{{- define "playerB.serviceAccountName" -}}
{{- if .Values.PlayerB.serviceAccount.create }}
{{- default (include "playerA.fullname" .) .Values.PlayerB.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.PlayerB.serviceAccount.name }}
{{- end }}
{{- end }}