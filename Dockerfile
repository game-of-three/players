FROM dockerproxy-iva.si.francetelecom.fr/openjdk:11-jre

LABEL Name="Players"
LABEL Description="Backend for Players service"
LABEL Maintainer="mahmoudragab726@gmail.com"
LABEL Url="https://gitlab.com/game-of-three/players"
COPY ./target/players*.jar players.jar

EXPOSE 8080

HEALTHCHECK --start-period=60s \
  CMD curl -f http://localhost:8080/actuator/health || exit 1

USER 1001
ENTRYPOINT ["java","-jar","/players.jar"]
